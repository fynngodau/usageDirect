## Donations

If you liked the project so much that you would like to give it money, you may do so here.

Donations reach me, Fynn Godau, directly, and are not tax-deductible.

### Wire transfer with SEPA

If you live within the SEPA space, this is the preferred way of donating as no fees are
deducted from the amount that I receive.

You may transfer funds to:

    Recipient            Fynn Godau
    IBAN                 DE37 4306 0967 1010 2633 00
    BIC                  GENODEM1GLS
    Reason for transfer  usageDirect donation

### Credit card via Stripe

Due to [the fees that Stripe takes from your donation](https://stripe.com/en-de/pricing),
it is preferable if you donate via wire transfer if you can.

* [Donate in USD](https://donate.stripe.com/28o8A38g8efYdgIfZ3)
* [Donate in EUR](https://donate.stripe.com/7sI3fJ8g87RAekM7su)
* If you need to donate in another currency, [contact me](mailto:fynngodau@mailbox.org),
  and I will provide you with a link.

