
# usageDirect

This project is in a low-maintenance state. This means that you are not guaranteed to receive responses to emails or issues, but I will look at your pull requests in due time. Occasionally, there may be a maintenance release.

This source builds the two apps "usageDirect" (`database` flavor) as well as "System usage stats" (`system` flavor, to be removed).

### [Download usageDirect from F-Droid](https://f-droid.org/packages/godau.fynn.usagedirect)

For more information, please refer to the F-Droid description and screenshots.

### [Help translate](https://translate.codeberg.org/projects/usagedirect/)

This project can be translated through codeberg's Weblate instance. Please see the following notes:

* To translate the About screen, please see [the library librariesDirect](https://translate.codeberg.org/projects/librariesDirect/librariesDirect).
* If everything is in sync, **every string should have a screenshot** or at least a note attached to it by me. **Please pay attention to the screenshots and string context information** and make sure your translation fits into the context seen in the screenshot.
* There are different modules for the app usageDirect, for System usage stats as well as for the code that is shared by both of these apps ("Common").
* Your weblate email address will be contained in the git log of this repository.
* If you would like to help maintain your translation in the future, please **watch** the project on weblate. You will then receive announcements via email prior to the release of a new version once strings are finalized.

Thank you a lot for your help.

---

Forked from [the Android sample called "AppUsageStatistics"](https://github.com/googlesamples/android-AppUsageStatistics)
(Copyright 2017 The Android Open Source Project, Inc).

This app uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in IntelliJ IDEA (or Android Studio).
